import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CharactersService } from '../../services/characters.service';
import { Result } from '../../interfaces/character.interface';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.less']
})
export class CharacterComponent implements OnInit {

  characters: Result[] = [];
  dic = new Map<string, string>();

  constructor(
    private router: Router, 
    private route: ActivatedRoute, 
    private charService: CharactersService
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(({ nombre }) => {
      if(nombre) {
        this.dic.clear();
        this.dic.set('nameStartsWith', nombre);
      }

      this.showAllCharacters();
    });
  }

  showAllCharacters() {
    this.charService.getCharacters(this.dic).subscribe((r) => {
      this.characters = r.data.results;
    });
  }

  seeMore(id: number) {
    this.router.navigate(['/character', id]);
  }
}
