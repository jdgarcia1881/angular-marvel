import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-character-search',
  templateUrl: './character-search.component.html',
  styleUrls: ['./character-search.component.less']
})
export class CharacterSearchComponent implements OnInit {

  searchCharacter: FormControl;

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.searchCharacter = new FormControl('', Validators.required);
  }

  onSubmit() {
    if(this.searchCharacter.valid) {
      this.router.navigate(['/characters'], { queryParams: { nombre: this.searchCharacter.value } });
      this.searchCharacter.reset();
    }
  }

}
