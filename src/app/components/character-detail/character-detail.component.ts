import { Component, OnInit } from '@angular/core';
import { Result, Character } from '../../interfaces/character.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { CharactersService } from '../../services/characters.service';

@Component({
  selector: 'app-character-detail',
  templateUrl: './character-detail.component.html',
  styleUrls: ['./character-detail.component.less']
})
export class CharacterDetailComponent implements OnInit {
  characters: Result[] = [];
  content: any;
  lista: any[];

  constructor( 
    private router: Router, 
    private activatedRoute: ActivatedRoute, 
    private charactersService: CharactersService
  ) {
  }

  ngOnInit(): void {

    this.activatedRoute.params.subscribe((params) => {
      this.charactersService.getCharacter(params.id).subscribe((r) => {
        this.characters = r.data.results;
        console.log(this.characters);
      });
    });

  }

  back() {
    this.router.navigate(['/characters']);
  }
}
