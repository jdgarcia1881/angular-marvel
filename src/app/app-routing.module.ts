import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CharacterComponent } from './components/character/character.component';
import { CharacterDetailComponent } from './components/character-detail/character-detail.component';
import { CharacterSearchComponent } from './components/character-search/character-search.component';

const routes: Routes = [
  { path: 'characters', component: CharacterComponent },
  { path: 'character/:id', component: CharacterDetailComponent },
  { path: 'characters/:query', component: CharacterSearchComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'characters'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
