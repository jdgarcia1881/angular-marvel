import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    
    request = request.clone(
      { setParams : {
          'apikey': '8d260a19a538386c30dc1bc676fd2cae',
          'hash': '48c6d160a1d1ab5d018b8ae54f07f559',
          'ts': '2000'
        }
      }
    );

    return next.handle(request);
  }
}
