import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Character, Result } from '../interfaces/character.interface';

@Injectable({
  providedIn: 'root'
})

export class CharactersService {

  constructor(private http: HttpClient) { }
  
  getCharacters(dic: Map<string, string>) {
    let params = new HttpParams();
    dic.forEach((value, key) => params = params.set(key, value));
    return this.http.get<Character>(environment.apiUrl + '/characters', {params});
  }

  getCharacter(id: number) {
    return this.http.get<Character>(environment.apiUrl + `/characters/${id}`);
  }
}
